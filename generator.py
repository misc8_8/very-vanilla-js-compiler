from parser import DefNode, CallNode, IntNode, VarRefNode, AdditionNode


class Generator:
    def generate_from(self, node):
        if type(node) is DefNode:
            return f"function {node.name}({','.join(node.arg_names)}) {{ return {self.generate_from(node.body)} }};"
        elif type(node) is CallNode:
            return f"{node.name}({(',').join(map(self.generate_from, node.arg_exprs))})"
        elif type(node) is AdditionNode:
            return f"{('+').join(node.arg_exprs)}"
        elif type(node) is IntNode or VarRefNode:
            return str(node.value)
        raise RuntimeError(f'Unexpected type: {type(node)}')
